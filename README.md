# scalelite-redis-stunnel

After installing `scalelite-poller`:

* Build Docker image: `docker build -t stunnel .`
* Configure `STUNNEL_CONNECT` (e.g. `redis.db-url.com:42000`), `STUNNEL_ACCEPT` (e.g. `9000`) and published ports (e.g. `9000:9000`) inside `systemd.service` file.
* Move systemd configuration to related path: `cp systemd.service /etc/systemd/system/scalelite-redis-stunnel.service`
* Configure `REDIS_URL` in `/etc/default/scalelite` (e.g. `redis://username:password@scalelite-redis-stunnel:PORT`).
* Reload all systemd unit files: `systemctl daemon-reload`
* Enable scalelite-redis-stunnel unit: `systemctl enable scalelite-redis-stunnel`
* Restart Scalelite: `systemctl restart scalelite.target`
* Check if `scalelite-poller` is running: `systemctl status scalelite-poller`
* If it is not running, investigate the reason.

## Credits:
* https://github.com/dweomer/dockerfiles-stunnel
