#!/bin/sh -e

export STUNNEL_CONF="/etc/stunnel/stunnel.conf"

if [[ ! -s ${STUNNEL_CONF} ]]; then
    cat /srv/stunnel/stunnel.conf.template | envsubst > ${STUNNEL_CONF}
fi

exec "$@"
